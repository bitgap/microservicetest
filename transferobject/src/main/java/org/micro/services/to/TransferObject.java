package org.micro.services.to;

import java.util.ArrayList;
import java.util.List;


public class TransferObject {
	
	private String objectname;
	private List<String> timestamps;
	
	public TransferObject() {
		timestamps = new ArrayList<String>();
	}
	
	public void addTimeStamp(String timestamp){
		timestamps.add(timestamp);
	}
	
	public List<String> getTimestamps() {
		return timestamps;
	}
	public void setTimestamps(List<String> timestamps) {
		this.timestamps = timestamps;
	}
	public String getObjectname() {
		return objectname;
	}
	public void setObjectname(String objectname) {
		this.objectname = objectname;
	}
	
}
