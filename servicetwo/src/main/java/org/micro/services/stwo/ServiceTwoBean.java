package org.micro.services.stwo;

import org.micro.services.to.TransferObject;

public class ServiceTwoBean {
	public TransferObject transfer(TransferObject message){
		message.addTimeStamp(this.getClass().getName() + ":" + System.currentTimeMillis());
		return message;
	}
}
