package org.micro.services.sfive;

import org.micro.services.to.TransferObject;

public class ServiceFiveBean {
	public TransferObject transfer(TransferObject message){
		message.addTimeStamp(this.getClass().getName() + ":" + System.currentTimeMillis());
		return message;
	}
}
