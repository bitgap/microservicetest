package org.micro.services.sfour;

import org.micro.services.to.TransferObject;

public class ServiceFourBean {
	public TransferObject transfer(TransferObject message){
		message.addTimeStamp(this.getClass().getName() + ":" + System.currentTimeMillis());
		return message;
	}
}
