import requests
import json
import time
#import numpy as np

url = 'http://0.0.0.0:8893/embedded'
url1 = 'http://0.0.0.0:8888/serviceone'
url2 = 'http://0.0.0.0:8889/servicetwo'
url3 = 'http://0.0.0.0:8890/servicethree'
url4 = 'http://0.0.0.0:8891/servicefour'
url5 = 'http://0.0.0.0:8892/servicefive'
data = '{ "objectname" : "python calling", "timestamps" :[] }'

def returnRestCallTimestamps(url, data):
        r = requests.post(url,data)
        return r.json()["timestamps"]


def calcDelta(timestamps):
    tmin = 0
    tmax = 0
    for i in timestamps:
        v = i.split(':')
        if tmin == 0: tmin = int(v[1])
        if tmax == 0: tmax = int(v[1])
        tmin = min(int(v[1]), tmin)
        tmax = max(int(v[1]), tmax)
    # print(str(tmax -tmin))
    return tmax-tmin

count = 1000
delays = []

if __name__ == "__main__":
    print("starting embedded service run")
    delays = []
    for i in range(1,count):
        tstart = time.time()
        ts = returnRestCallTimestamps(url, data)
        tstop = time.time()
        delay = calcDelta(ts)
        delays.append(tstop -tstart)
        # print(tstop -tstart)

    averageEmbedded = sum(delays)/count
    print("embedded result : " + str(averageEmbedded))
    delays = []
    print("starting dedicated service run")
    for i in range(1,count):
        tstart = time.time()
        ts = returnRestCallTimestamps(url4, data)
        tstop = time.time()
        delay = calcDelta(ts)
        delays.append(tstop -tstart)
        # print(tstop -tstart)

    averageDedicated = sum(delays)/count
    print("dedicated on one box result : " + str(averageDedicated))

    print("*"*100)
    print("difference dedicated/embedded : " + str(averageDedicated/averageEmbedded))



