package org.micro.services.routes;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.spi.DataFormat;
import org.micro.services.sfive.ServiceFiveBean;
import org.micro.services.sfour.ServiceFourBean;
import org.micro.services.sone.ServiceOneBean;
import org.micro.services.sthree.ServiceThreeBean;
//import org.micro.services.to.TransferObject;
import org.micro.services.stwo.ServiceTwoBean;
import org.micro.services.to.TransferObject;

public class ServerOneRoutes extends RouteBuilder {
	DataFormat json = new JacksonDataFormat(TransferObject.class);

	@Override
	public void configure() throws Exception {
		from("jetty://http://0.0.0.0:8888/serviceone")
				.process(new Processor() {
					
					@Override
					public void process(Exchange exchange) throws Exception {
						exchange.getIn().setHeader("CamelJacksonUnmarshalType", "org.micro.services.to.TransferObject");
					}
				})
				.unmarshal(json)
				.bean(new ServiceOneBean(), "transfer(TransferObject)")
				.marshal("json")
				.to("http://0.0.0.0:8889/servicetwo?bridgeEndpoint=true");
	}

}
