package org.micro.services.sone;

import org.micro.services.to.TransferObject;

public class ServiceOneBean {
	
	public TransferObject transfer(TransferObject message){
		// do some processing
//		System.out.println("so far so good: " + message.getObjectname());
//		for (String ts : message.getTimestamps()) {
//			System.out.println("timestamp: " + ts);
//		}
		//TransferObject message = new TransferObject();
		//message.setObjectname("ServiceOne");
		message.addTimeStamp(this.getClass().getName() + ":" + System.currentTimeMillis());
		return message;
	}

}
