package org.micro.services.routes;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.spi.DataFormat;
import org.micro.services.sfive.ServiceFiveBean;
import org.micro.services.sfour.ServiceFourBean;
import org.micro.services.sone.ServiceOneBean;
import org.micro.services.sthree.ServiceThreeBean;
//import org.micro.services.to.TransferObject;
import org.micro.services.stwo.ServiceTwoBean;
import org.micro.services.to.TransferObject;

// simple message router that mimics the daisy chain call impact of microservices
public class EmbeddedTest extends RouteBuilder {
	DataFormat json = new JacksonDataFormat(TransferObject.class);

	@Override
	public void configure() throws Exception {
		from("jetty://http://0.0.0.0:8000/serviceone")
				.process(new Processor() {
					
					@Override
					public void process(Exchange exchange) throws Exception {
						exchange.getIn().setHeader("CamelJacksonUnmarshalType", "org.micro.services.to.TransferObject");
					}
				})
				.unmarshal(json)
				.bean(new ServiceOneBean(), "transfer(TransferObject)")
				.marshal("json")
				.to("http://0.0.0.0:8001/servicetwo?bridgeEndpoint=true");

		from("jetty://http://0.0.0.0:8001/servicetwo")
				.process(new Processor() {
					
					@Override
					public void process(Exchange exchange) throws Exception {
						exchange.getIn().setHeader("CamelJacksonUnmarshalType", "org.micro.services.to.TransferObject");
					}
				})
				.unmarshal(json)
				.bean(new ServiceTwoBean(), "transfer(TransferObject)")
				.marshal("json")
				.to("http://0.0.0.0:8002/servicethree?bridgeEndpoint=true");

		
		from("jetty://http://0.0.0.0:8002/servicethree")
				.process(new Processor() {
					
					@Override
					public void process(Exchange exchange) throws Exception {
						exchange.getIn().setHeader("CamelJacksonUnmarshalType", "org.micro.services.to.TransferObject");
					}
				})
				.unmarshal(json)
				.bean(new ServiceThreeBean(), "transfer(TransferObject)")
				.marshal("json")
				.to("http://0.0.0.0:8003/servicefour?bridgeEndpoint=true");
		
		from("jetty://http://0.0.0.0:8003/servicefour")
				.process(new Processor() {
					
					@Override
					public void process(Exchange exchange) throws Exception {
						exchange.getIn().setHeader("CamelJacksonUnmarshalType", "org.micro.services.to.TransferObject");
					}
				})
				.unmarshal(json)
				.bean(new ServiceFourBean(), "transfer(TransferObject)")
				.marshal("json")
				.to("http://0.0.0.0:8004/servicefive?bridgeEndpoint=true");
		
		from("jetty://http://0.0.0.0:8004/servicefive")
				.process(new Processor() {
					
					@Override
					public void process(Exchange exchange) throws Exception {
						exchange.getIn().setHeader("CamelJacksonUnmarshalType", "org.micro.services.to.TransferObject");
					}
				})
				.unmarshal(json)
				.bean(new ServiceFiveBean(), "transfer(TransferObject)")
				.marshal("json");
		
		from("jetty://http://0.0.0.0:8893/embedded")
				.process(new Processor() {
					
					@Override
					public void process(Exchange exchange) throws Exception {
						exchange.getIn().setHeader("CamelJacksonUnmarshalType", "org.micro.services.to.TransferObject");
					}
				})
				.unmarshal(json)
				.bean(new ServiceOneBean(), "transfer(TransferObject)")
				.bean(new ServiceTwoBean(), "transfer(TransferObject)")
				.bean(new ServiceThreeBean(), "transfer(TransferObject)")
				.bean(new ServiceFourBean(), "transfer(TransferObject)")
				.bean(new ServiceFiveBean(), "transfer(TransferObject)")
				.marshal("json");
	}

}
