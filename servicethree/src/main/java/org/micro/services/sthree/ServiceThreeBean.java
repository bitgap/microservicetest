package org.micro.services.sthree;

import org.micro.services.to.TransferObject;

public class ServiceThreeBean {
	public TransferObject transfer(TransferObject message){
		message.addTimeStamp(this.getClass().getName() + ":" + System.currentTimeMillis());
		return message;
	}
}
