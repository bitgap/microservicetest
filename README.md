# README #

Simple project to see how "microservice based architecture" feels and scales. 

# What is this repository for? #

* MicroService Example
* Version 0.0.1


# How to build #
* git clone ...
* cd parentpom
* mvn install

# Configuration #
* Currently everything in code
* Aim is to factor out configuration into Camel properties file ... TODO
* Jetty needs to be configured (heap size, ports .....)

# Dependencies #
Java & Maven & python (optional)

# How to run #
## Embedded Services ##
For the embeddedServer (all services wired via Code reuse instead of Runtime reuse) just 

* cd embeddedServer
* mvn [-Djetty:port=XZY] mvn:jetty
* use the pyclient.py change it as you like and run your test or
* use Chrome/Postman | curl to call the first microservice
* do a rest post call with the following data:

```
#!javascript

{
 "objectname" : "anyname", "timestamps" :[] 
}
```

and use e.g. http://localhost:8893/embedded resulting into 

```
#!javascript

{"objectname":"anyname","timestamps":["org.micro.services.sone.ServiceOneBean:1443267337363","org.micro.services.stwo.ServiceTwoBean:1443267337366","org.micro.services.sthree.ServiceThreeBean:1443267337368","org.micro.services.sfour.ServiceFourBean:1443267337369","org.micro.services.sfive.ServiceFiveBean:1443267337370"]}
```


## Standalone Services(Servers) ##
For the dedicated server aka microservice setup just follow the steps

* cd in each server.../ folder [one to five]
* mvn -Djetty:port[9000..9004] jetty:run & //required as jetty needs a general port 
* once done use Chrome/Postman | curl to call the first microservice
* do a rest post call with the following data:

```
#!javascript

{
 "objectname" : "anyname", "timestamps" :[] 
}
```

and use e.g. http://localhost:8888/serviceone

This will call serviceone -> servicetwo -> servicethree -> servicefour -> servicefive and returns a json structure e.g. 


```
#!javascript

{"objectname":"message1","timestamps":["org.micro.services.sone.ServiceOneBean:1443267121919","org.micro.services.stwo.ServiceTwoBean:1443267122886","org.micro.services.sthree.ServiceThreeBean:1443267122954","org.micro.services.sfour.ServiceFourBean:1443267122908","org.micro.services.sfive.ServiceFiveBean:1443267122977"]}
```

each service with its timestamp (ms)


# Code vs. Runtime reuse #
* latency is higher as expected due to marshalling/unmarshalling 
* about 3 times higher on the local box
* about 6 times higher on two boxes with one router inbetween
* not tested horizontal scaling by adding load-balancers e.g. HAProxy etc (another brick in the latency chain) 
* add the encryption/decryption costs to the solution (for every remote call)
* one of the big microservice promises is to ease software development by splitting complex things into smaller reusable services each in its own process but there is usually a **but** 
* the decomposition of software into smaller reusable components/services is an old best practices, so no big deal to create 5 Service[One,Two,Three,Four,Five]Beans jars and there reuse in other components thanks to mavens dependency management
* to create another five Server[One,Two,Three,Four,Five] items was the extra mile for a microservice architecture
* starting/stopping the embedded approach was simple and wiring trivial by simply calling methods
* starting/stopping the microservice style is a pain without tools that can help to do all the wiring and system management
* resource impact is higher if not carefully tuned (another extra mile) 
* monitoring is simpler for less units 
* debugging is as expected much simpler in the embedded case 
* logfile analysis requires a solution like Splunk/ELK, otherwise it becomes a nightmare
* and the logfile analysis requires a good knowledge of message flows across the distributed application 

# Conclusion #
Overall the microservice architectur comes with a lot of extra miles and I would recommend for the standard cases to stick to code reuse instead of runtime reuse. That doesn't mean to neglect the advantage of decomposition into reusable components but prefer a runtime container that is engineered and optimised for that, some examples

## J2EE Container ##
* It is easy to have N-ears aka J2EE applications in one container
* use in process communication among those based on shared libraries or local ejb-calls
* comes with all the NFRs (Security, Threading, Resources ....) done

## OSGi Container ##
* OSGi is more dynamic when it comes to service replacement/upgrade/loading
* so better suited for the microservice approach 
* comes also with all NFRs addressed

## Erlang/OTP (my personal preference) ##
* if someone wants really scale-up and build 5-9 nines availability systems just look into this technology
* it comes with fault-tolerance build into its core
* it comes with supervision trees that can keep your processes going
* it scales across hundreds of servers
* it supports 2 generations of code aka hot code reloading (see nine nines) 
* it comes with process wiring
* it is completely process based (objects become processes)
* no shared state design 
* functional programming
* fault-tolerance build in in the sense **let it crash**
* see showcases like WhatsApp([19.5 Billion Architecture](http://highscalability.com/blog/2014/2/26/the-whatsapp-architecture-facebook-bought-for-19-billion.html)), GitHub, RabbitMQ, eJabberD those things that really scale

## Previous Microservice Designs ##
I worked in a company 2 decades ago on SunOS (C/C++/Java) and we used a process based architecture pretty close to the ideas of microservice architecture and it worked quite fine and scaled well. It was for realtime market data streams and subscriptions. And one key design element was **shared memory** and intense use of **Mutex/Semaphores....**. Due to the unix process based design it was very robust. It scaled tremendously good as the core was later used for video stream handling at Airports. But it also required discipline from all process designers not to mess up the shared memory. For any microservice architect I recommend to look into shared memory/in-process communication to address the network costs.  

# Things to look for #
In order to reuse hundreds of microservices one needs to model and execute flows like the often referenced Unix pipe example. So how should this work? 

* manual configuration doesn't scale and turns maintainance into a nightmare
* monitoring ... uuuhhh a tough thing 

I recommend to use a message routing system/framework like Apache Camel, Spring Integration etc. as this provides tools and concepts for all those non-obvious aspects


# Why I did it #
Basically because I had no facts but some gut feelings. Being in technical architecture for more than 20 years I see many new concepts/styles show up which overlap and claim to be the solution for long-standing problems (sometimes unclear which problem gets solved/addressed) .